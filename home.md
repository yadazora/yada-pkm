Hello & Welcome to Yada Notes  Stupid People in AI
[NOTE TAKING APPROACH]
To Learn how to contribute your personal notes read below course outline

**PATH 1**![[robot.jpg]]
-   Module 1 – Preparatory Sessions – Python & Linux
-   Module 2 – GIT
-   Module 3 – Python with Data Science
-   Module 4 – Advanced Statistics
-   Module 5 – Machine Learning & Prediction Algorithms
-   Module 6 – Data Science at Scale with PySpark
-   Module 7 – AI & Deep Learning using TensorFlow
-   Module 8 – Deploying Machine Learning Models on Cloud (MLOps)
-   Module 9 – Data Visualization with Tableau
-   Module 10 – Data Science Capstone Project
-   Module 11 – Data Analysis with MS Excel
-   Module 12 – Data Wrangling with SQL
-   Module 13 – Natural Language Processing and its Applications

**PATH 2**
-   **Foundation**
    
    -   Python for AI & ML
    
    -   Applied Statistics
-   **Machine Learning**
    
    -   Supervised Learning
    
    -   Unsupervised Learning
    
    -   Ensemble Techniques
    
    -   Featurization, Model Selection & Tuning
    
    -   Recommendation Systems
-   **Artificial Intelligence**
    
    -   Introduction to Neural Networks and Deep Learning
    
    -   [**Computer Vision**](https://intellipaat.com/blog/what-is-computer-vision/)
    
    -   Natural Language Processing
-   **Additional Modules**
    
    -   EDA
    
    -   Time Series Forecasting
    
    -   Pre Work for Deep Learning
    
    -   Model Deployment
    
    -   Visualization using Tensor board
    
    -   GANs (Generative Adversarial Networks)
    
    -   Reinforcement Learning